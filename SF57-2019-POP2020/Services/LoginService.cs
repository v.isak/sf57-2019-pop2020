﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF57_2019_POP2020.Models;

namespace SF57_2019_POP2020.Services
{
    public class LoginService
    {
        public static Korisnik prijava(string jmbg, string lozinka)
        {
            Korisnik pronadjenKorisnik = Util.Instance.Korisnici.ToList().Find(k => k.JMBG.Equals(jmbg) && k.Lozinka.Equals(lozinka) && k.Aktivan == true);
            return pronadjenKorisnik;
        }
    }
}
