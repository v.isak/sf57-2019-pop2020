﻿using SF57_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF57_2019_POP2020.Services
{
    public static class PodaciService
    {

        public static ObservableCollection<Termin> aktivniTermini()
        {
            ObservableCollection<Termin> aktivniTermini = new ObservableCollection<Termin>();
            foreach (Termin termin in Util.Instance.listaTermina)
            {
                if (termin.Aktivan)
                {
                    aktivniTermini.Add(termin);
                }
            }
            return aktivniTermini;
        }

        public static ObservableCollection<Korisnik> aktivniKorisnici()
        {
            ObservableCollection<Korisnik> aktivniKorisnici = new ObservableCollection<Korisnik>();
            foreach (Korisnik korisnik in Util.Instance.Korisnici)
            {
                if (korisnik.Aktivan)
                {
                    aktivniKorisnici.Add(korisnik);
                }
            }
            return aktivniKorisnici;
        }

        public static ObservableCollection<DomZdravlja> aktivniDZ()
        {
            ObservableCollection<DomZdravlja> aktivniDZ = new ObservableCollection<DomZdravlja>();
            foreach (DomZdravlja domZdravlja in Util.Instance.listaDomovaZdravlja)
            {
                if (domZdravlja.Aktivan)
                {
                    aktivniDZ.Add(domZdravlja);
                }
            }
            return aktivniDZ;
        }

        public static ObservableCollection<Terapija> aktivneTerapije()
        {
            ObservableCollection<Terapija> aktivneTerapije = new ObservableCollection<Terapija>();
            foreach (Terapija terapija in Util.Instance.listaTerapija)
            {
                if (terapija.Aktivan)
                {
                    aktivneTerapije.Add(terapija);
                }
            }
            return aktivneTerapije;
        }


    }
}
