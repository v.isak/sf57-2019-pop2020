﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF57_2019_POP2020.Models
{
    [Serializable]
    public class Lekar 
    {
        private DomZdravlja _domZdravlja;

        public DomZdravlja DomZdravlja
        {
            get { return _domZdravlja; }
            set { _domZdravlja = value; }
        }

        private Korisnik _korisnik;

        public Korisnik Korisnik
        {
            get { return _korisnik; }
            set { _korisnik = value; }
        }

        public override string ToString()
        {
            return Korisnik.KorisnickoIme;
        }

        public string LekarUpisUFajl()
        {
            return DomZdravlja.ID + ";" + Korisnik.KorisnickoIme;
        }

    }
}
