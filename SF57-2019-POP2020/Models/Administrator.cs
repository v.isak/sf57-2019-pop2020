﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF57_2019_POP2020.Models
{
    public class Administrator
    {
        private Korisnik _korisnik;

        public Korisnik Korisnik
        {
            get { return _korisnik; }
            set { _korisnik = value; }
        }


        private ObservableCollection<DomZdravlja> _listaDZ;

        public ObservableCollection<DomZdravlja> ListaDZ
        {
            get { return _listaDZ; }
            set { _listaDZ = value; }
        }


        private ObservableCollection<Korisnik> _listaKorisnika;
        
        public ObservableCollection<Korisnik> ListaKorisnika
        {
            get { return _listaKorisnika; }
            set { _listaKorisnika = value; }
        }


        private ObservableCollection<Termin> _listaTermina;

        public ObservableCollection<Termin> ListaTermina
        {
            get { return _listaTermina; }
            set { _listaTermina = value; }
        }

        private ObservableCollection<Terapija> _listaTerapija;

        public ObservableCollection<Terapija> ListaTerapija
        {
            get { return _listaTerapija; }
            set { _listaTerapija = value; }
        }
    }
}
