﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF57_2019_POP2020.Models
{
    [Serializable]
    public class DomZdravlja
    {
        private string _id;

        public string ID
        {
            get { return _id; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    if (Util.Instance.listaDomovaZdravlja.ToList().Find(k => k.ID.Equals(value)) == null)
                    {
                        _id = value;
                    }
                }
            }
        }

        private string _naziv;

        public string Naziv
        {
            get { return _naziv; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    _naziv = value;
                }
            }
        }

        private Adresa _adresa;

        public Adresa Adresa
        {
            get { return _adresa; }
            set { _adresa = value; }
        }

        private bool _aktivan;

        public bool Aktivan
        {
            get { return _aktivan; }
            set { _aktivan = value; }
        }

        public override string ToString()
        {
            return Naziv;//"Naziv: " + this.Naziv + " Adresa: " + this.Adresa.ToString();
        }
    }
}
