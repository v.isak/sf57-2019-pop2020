﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF57_2019_POP2020.Models
{
    [Serializable]

    public class Pacijent
    {

        private Korisnik _korisnik;
        private ObservableCollection<Termin> _listaTermina;
        private ObservableCollection<Terapija> _zdravstveniKarton;

        public Korisnik Korisnik
        {
            get { return _korisnik; }
            set { _korisnik = value; }
        }

        public ObservableCollection<Termin> ListaTermina
        {
            get { return _listaTermina; }
            set { _listaTermina = value; }
        }

        public ObservableCollection<Terapija> ZdravstveniKarton
        {
            get { return _zdravstveniKarton; }
            set { _zdravstveniKarton = value; }
        }


        public override string ToString()
        {
            return Korisnik.KorisnickoIme;
        }
    }
}
