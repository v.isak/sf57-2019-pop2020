﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF57_2019_POP2020.Models
{
    [Serializable]
    public class Terapija
    {
        private string _id;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _opis;

        public string Opis
        {
            get { return _opis; }
            set { _opis = value; }
        }

        private Lekar _lekar;

        public Lekar Lekar
        {
            get { return _lekar; }
            set { _lekar = value; }
        }

        private bool _aktivan;

        public bool Aktivan
        {
            get { return _aktivan; }
            set { _aktivan = value; }
        }

        public override string ToString()
        {
            return "Opis: " + this.Opis + " Lekar: " + this.Lekar.Korisnik.KorisnickoIme;
        }
    }
}
