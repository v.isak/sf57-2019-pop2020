﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using SF57_2019_POP2020.Services;

namespace SF57_2019_POP2020.Models
{
    public sealed class Util
    {
        private static readonly Util instance = new Util();
        readonly IUserService _userService;
        readonly IUserService _doctorService;

        private Util()
        {
            _userService = new UserService();
            _doctorService = new DoctorService();
        }
        static Util()
        {
            
        }

        public static Util Instance
        {
            get
            {
                return instance;
            }
        }

        public string ID = "0";
        public List<string> listaGradova { get; set; }
        public ObservableCollection<DomZdravlja> listaDomovaZdravlja { get; set; }
        public ObservableCollection<Adresa> listaAdresa { get; set; }
        public ObservableCollection<Termin> listaTermina { get; set; }
        public ObservableCollection<Terapija> listaTerapija { get; set; }
        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Lekar> Lekari { get; set; }
        public ObservableCollection<Pacijent> Pacijenti { get; set; }
        public ObservableCollection<Administrator> Admini { get; set; }

        public void Initialize()
        {
            listaDomovaZdravlja = new ObservableCollection<DomZdravlja>();
            listaAdresa = new ObservableCollection<Adresa>();
            listaGradova = new List<string>();
            Korisnici = new ObservableCollection<Korisnik>();
            Lekari = new ObservableCollection<Lekar>();
            Pacijenti = new ObservableCollection<Pacijent>();
            Admini = new ObservableCollection<Administrator>();
            listaTermina = new ObservableCollection<Termin>();
            listaTerapija = new ObservableCollection<Terapija>();

            Adresa adresa = new Adresa
            {
                Grad = "Grad 1",
                Broj = "1",
                Drzava = "Drzava 1",
                Ulica = "Ulica 1",
                ID = "1",
                Aktivan = true
            };

            Adresa adresa1 = new Adresa
            {
                Grad = "Grad 2",
                Broj = "1",
                Drzava = "Drzava 1",
                Ulica = "Ulica 1",
                ID = "2",
                Aktivan = true
            };

            Adresa adresa2 = new Adresa
            {
                Grad = "Grad 1",
                Broj = "2",
                Drzava = "Drzava 2",
                Ulica = "Ulica 2",
                ID = "3",
                Aktivan = true
            };
            listaAdresa.Add(adresa);
            listaAdresa.Add(adresa1);
            listaAdresa.Add(adresa2);

            DomZdravlja dz = new DomZdravlja
            {
                ID = "iddz",
                Naziv = "Dom zdravlja",
                Adresa = adresa,
                Aktivan = true
            };
            DomZdravlja dz1 = new DomZdravlja
            {
                ID = "iddz1",
                Naziv = "Dom zdravlja 1",
                Adresa = adresa1,
                Aktivan = true
            };

            DomZdravlja dz2 = new DomZdravlja
            {
                ID = "iddz2",
                Naziv = "Dom zdravlja 2",
                Adresa = adresa2,
                Aktivan = true
            };

            listaDomovaZdravlja.Add(dz);
            listaDomovaZdravlja.Add(dz1);
            listaDomovaZdravlja.Add(dz2);

            foreach (Adresa ad in listaAdresa)
            {
                if(!listaGradova.Contains(ad.Grad)){
                    listaGradova.Add(ad.Grad);
                }
            }

            Korisnik korisnik1 = new Korisnik();
            korisnik1.KorisnickoIme = "pera";
            korisnik1.Ime = "petar";
            korisnik1.Prezime = "peric";
            korisnik1.JMBG = "123456";
            korisnik1.Lozinka = "pera";
            korisnik1.Email = "pera@gmail.com";
            korisnik1.Pol = EPol.M;
            korisnik1.TipKorisnika = ETipKorisnika.LEKAR;
            korisnik1.Aktivan = true;
            korisnik1.Adresa = adresa;

            Korisnik korisnik2 = new Korisnik
            {
                Email = "zika@gmail.com",
                Ime = "zika",
                Prezime = "zikic",
                KorisnickoIme = "ziza",
                JMBG = "654321",
                Lozinka = "zika",
                Pol = EPol.Z,
                TipKorisnika = ETipKorisnika.PACIJENT,
                Adresa = adresa,
                Aktivan = true
            };

            Korisnik korisnik3 = new Korisnik
            {
                Email = "zika@gmail.com",
                Ime = "zika",
                Prezime = "zikic",
                KorisnickoIme = "ziza",
                JMBG = "123456",
                Lozinka = "ziza",
                Pol = EPol.Z,
                TipKorisnika = ETipKorisnika.ADMINISTRATOR,
                Adresa = adresa,
                Aktivan = true
            };

            Lekar lekar = new Lekar
            {
                DomZdravlja = dz,
                Korisnik = korisnik1
            };

            Terapija terapija = new Terapija
            {
                ID = "ter",
                Lekar = lekar,
                Opis = "opis",
                Aktivan = true
            };
            Terapija terapija1 = new Terapija
            {
                ID = "ter",
                Lekar = lekar,
                Opis = "opis",
                Aktivan = true
            };

            Pacijent pacijent = new Pacijent
            {
                Korisnik = korisnik2,
                ListaTermina = new ObservableCollection<Termin>(),
                ZdravstveniKarton = new ObservableCollection<Terapija>()
            };

            

            Termin termin = new Termin
            {
                ID = "terminid",
                Datum = DateTime.Now.AddMinutes(5.0),
                Lekar = lekar,
                Pacijent = null,
                StatusTermina = EStatusTermina.Slobodan,
                Aktivan = true
            };

            

            Korisnici.Add(korisnik1);
            Korisnici.Add(korisnik2);
            Korisnici.Add(korisnik3);

            Lekari.Add(lekar);
            pacijent.ZdravstveniKarton.Add(terapija);
            pacijent.ZdravstveniKarton.Add(terapija1);
            Pacijenti.Add(pacijent);
            listaTermina.Add(termin);
            listaTerapija.Add(terapija);
            listaTerapija.Add(terapija1);


            Administrator admin = new Administrator
            {
                ListaKorisnika = Util.Instance.Korisnici,
                ListaTerapija = Util.Instance.listaTerapija,
                ListaTermina = Util.Instance.listaTermina,
                ListaDZ = Util.Instance.listaDomovaZdravlja,
                Korisnik = korisnik3
            };
            Admini.Add(admin);

            
        }

        public void SacuvajEntite(string filename)
        {
            if(filename.Contains("korisnici"))
            {
                _userService.saveUsers(filename);
            }
            else if(filename.Contains("lekari"))
            {
                _doctorService.saveUsers(filename);
            }
        }

        public void CitanjeEntiteta(string filename)
        {
            if (filename.Contains("korisnici"))
            {
                _userService.readUsers(filename);
            }
            else if (filename.Contains("lekari"))
            {
                _doctorService.readUsers(filename);
            }
        }
    
        public void DeleteUser(string username)
        {
            _userService.deleteUser(username);
        }
    }
}
