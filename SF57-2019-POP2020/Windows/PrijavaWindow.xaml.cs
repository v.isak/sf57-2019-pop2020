﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF57_2019_POP2020.Models;
using SF57_2019_POP2020.Services;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for PrijavaWindow.xaml
    /// </summary>
    public partial class PrijavaWindow : Window
    {
        private Window HomeWindow;
        public PrijavaWindow(Window win)
        {
            HomeWindow = win;
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            Korisnik pronadjenKorisnik = LoginService.prijava(tbJMBG.Text, pbLozinka.Password); // LoginService

            if (pronadjenKorisnik != null) {
                this.Hide();
                HomeWindow.Close();
                if (pronadjenKorisnik.TipKorisnika == ETipKorisnika.PACIJENT)
                {
                    Pacijent pacijent = Util.Instance.Pacijenti.ToList().Find(k => k.Korisnik.JMBG.Equals(pronadjenKorisnik.JMBG));
                    GostWindow window = new GostWindow(pacijent);
                    window.Show();
                }
                else if (pronadjenKorisnik.TipKorisnika == ETipKorisnika.LEKAR)
                {
                    Lekar lekar = Util.Instance.Lekari.ToList().Find(k => k.Korisnik.JMBG.Equals(pronadjenKorisnik.JMBG));
                    LekarWindow window = new LekarWindow(lekar.Korisnik);
                    window.Show();
                }
                else // ADMIN
                {
                    Administrator admin = Util.Instance.Admini.ToList().Find(k => k.Korisnik.JMBG.Equals(pronadjenKorisnik.JMBG));
                    AdminWindow window = new AdminWindow(admin);
                    window.Show();
                }
            }

            else
            {
                MessageBox.Show("Uneli ste neispravne podatke!");
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
