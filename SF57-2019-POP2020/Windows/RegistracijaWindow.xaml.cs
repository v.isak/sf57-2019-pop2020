﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF57_2019_POP2020.Models;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for RegistracijaWindow.xaml
    /// </summary>
    public partial class RegistracijaWindow : Window
    {
        Korisnik korisnik;
        public RegistracijaWindow(Korisnik korisnik = null, Administrator admin = null)
        {
            this.DataContext = korisnik;
            this.korisnik = korisnik;
            InitializeComponent();
            txtAdresa.IsReadOnly = true;
            cbPol.ItemsSource = Enum.GetValues(typeof(EPol)).Cast<EPol>();
            cbTipKor.ItemsSource = Enum.GetValues(typeof(ETipKorisnika)).Cast<ETipKorisnika>();
            cbTipKor.IsEnabled = false;

            // NEREGISTROVANI
            if (korisnik == null)
            {
                cbPol.SelectedItem = EPol.M;
                cbTipKor.SelectedItem = ETipKorisnika.PACIJENT;
            }

            // REGISTROVANI
            else {
                Title = "Izmena";
                /*
                txtIme.Text = this.korisnik.Ime;
                txtPrezime.Text = this.korisnik.Prezime;
                txtJMBG.Text = this.korisnik.JMBG;
                txtEmail.Text = this.korisnik.Email;
                txtKorIme.Text = this.korisnik.KorisnickoIme;
                txtLozinka.Text = this.korisnik.Lozinka;
                */
                cbPol.SelectedItem = this.korisnik.Pol;
                cbTipKor.SelectedItem = this.korisnik.TipKorisnika;
                txtAdresa.Text = this.korisnik.Adresa.ToString();
                cbPol.IsEnabled = false;
                txtJMBG.IsReadOnly = true;
                txtKorIme.IsReadOnly = true;
                BtnPotvrda.Visibility = Visibility.Hidden;

                if (admin != null)
                {
                    txtKorIme.IsReadOnly = false;
                    txtJMBG.IsReadOnly = false;

                    if(korisnik.TipKorisnika == ETipKorisnika.PACIJENT)
                    {
                        BtnDodajLekara.Visibility = Visibility.Visible;
                        cbDZ.ItemsSource = admin.ListaDZ;
                        cbDZ.Visibility = Visibility.Visible;
                    }
                }
            }

        }

        private void btnAdresa_Click(object sender, RoutedEventArgs e)
        {
            AdresaWindow window;
            if (this.korisnik != null)
            {
                window = new AdresaWindow(this, korisnik.Adresa);
            }
            else
            {
                window = new AdresaWindow(this);
            }
            window.Show();
        }

        private void BtnPotvrda_Click(object sender, RoutedEventArgs e)
        {
            if (korisnik == null) // dodavanje novog korisnika
            {
                if (!greska())
                {
                    string[] ad = txtAdresa.Text.Split(',');
                    Adresa adresa = new Adresa
                    {
                        Drzava = ad[0],
                        Grad = ad[1],
                        Ulica = ad[2],
                        Broj = ad[3]
                    };
                    this.korisnik = new Korisnik
                    {
                        Ime = txtIme.Text,
                        Prezime = txtPrezime.Text,
                        JMBG = txtJMBG.Text,
                        Email = txtEmail.Text,
                        KorisnickoIme = txtKorIme.Text,
                        Lozinka = txtLozinka.Text,
                        Adresa = adresa,
                        Pol = (EPol)cbPol.SelectedItem,
                        TipKorisnika = ETipKorisnika.PACIJENT,
                        Aktivan = true
                    };
                    Pacijent pacijent = new Pacijent
                    {
                        Korisnik = this.korisnik,
                        ListaTermina = new ObservableCollection<Termin>(),
                        ZdravstveniKarton = new ObservableCollection<Terapija>()
                    };
                    Util.Instance.Korisnici.Add(korisnik);
                    Util.Instance.Pacijenti.Add(pacijent);
                    Util.Instance.listaAdresa.Add(adresa);
                    this.Close();
                }
            }
        }

        private bool greska()
        {
            string msg = "Popravite sledece:";
            bool greska = false;

            if (string.IsNullOrWhiteSpace(txtIme.Text))
            {
                greska = true;
                msg += "\nUnesite ime.";         
            }
            if (string.IsNullOrWhiteSpace(txtPrezime.Text))
            {
                greska = true;
                msg += "\nUnesite prezime.";
            }
            if (string.IsNullOrWhiteSpace(txtKorIme.Text))
            {
                greska = true;
                msg += "\nUnesite korisnicko ime.";
            }
            if (Util.Instance.Korisnici.ToList().Find(k => k.KorisnickoIme.Equals(txtKorIme.Text) && k.Aktivan == true) != null)
            {
                greska = true;
                msg += "\nKorisnicko ime vec postoji.";
            }
            if (string.IsNullOrWhiteSpace(txtLozinka.Text))
            {
                greska = true;
                msg += "\nUnesite lozinku.";
            }
            if (string.IsNullOrWhiteSpace(txtJMBG.Text))
            {
                greska = true;
                msg += "\nUnesite JMBG.";
            }
            if (Util.Instance.Korisnici.ToList().Find(k => k.JMBG.Equals(txtJMBG.Text) && k.Aktivan==true) != null)
            {
                greska = true;
                msg += "\nJMBG vec postoji.";
            }
            if (string.IsNullOrWhiteSpace(txtEmail.Text))
            {
                greska = true;
                msg += "\nUnesite email.";
            }
            if (string.IsNullOrWhiteSpace(txtAdresa.Text))
            {
                greska = true;
                msg += "\nUnesite adresu.";
            }

            if(greska == true)
            {
                MessageBox.Show(msg, "Greska");
            }

            return greska;
        }

        private void BtnDodajLekara_Click(object sender, RoutedEventArgs e)
        {
            if (cbDZ.SelectedItem != null)
            {
                korisnik.TipKorisnika = ETipKorisnika.LEKAR;
                Lekar lekar = new Lekar
                {
                    Korisnik = korisnik,
                    DomZdravlja = cbDZ.SelectedItem as DomZdravlja
                };
                Util.Instance.Pacijenti.Remove(Util.Instance.Pacijenti.ToList().Find(k => k.Korisnik.JMBG.Equals(korisnik.JMBG)));
                Util.Instance.Lekari.Add(lekar);
                MessageBox.Show("Korisnik je uspesno dodat u lekare.");
                this.Close();
            }
            else
            {
                MessageBox.Show("Odaberite dom zdravlja u kome radi lekar.");
            }
        }
    }
}
