﻿using SF57_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for GostWindow.xaml
    /// </summary>
    public partial class GostWindow : Window
    {
        private CollectionViewSource cvs = new CollectionViewSource();
        private CollectionViewSource cvs1 = new CollectionViewSource();
        private Pacijent pacijent;
        public GostWindow(Pacijent pacijent = null)
        {
            this.pacijent = pacijent;
            InitializeComponent();

            cbGrad.ItemsSource = Util.Instance.listaGradova;
            //cbGrad.IsSynchronizedWithCurrentItem = true;

            cvs.Source = Util.Instance.listaDomovaZdravlja;
            cvs.Filter += new FilterEventHandler(DZFilter);
            dgDZ.ItemsSource = cvs.View;
            dgDZ.AutoGenerateColumns = true;
            dgDZ.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            cvs1.Source = Util.Instance.Lekari;
            cvs1.Filter += new FilterEventHandler(LekariFilter);
            dgLekari.ItemsSource = cvs1.View;
            dgLekari.AutoGenerateColumns = true;
            dgLekari.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            if (pacijent != null)
            {
                Title = "Korisnik: " + pacijent.Korisnik.KorisnickoIme;
                BtnReg.Visibility = Visibility.Hidden;
                BtnZK.Visibility = Visibility.Visible;
                BtnOdjava.Visibility = Visibility.Visible;
                BtnPrikazTermina.Visibility = Visibility.Visible;
                Menu.Visibility = Visibility.Visible;
            }
        }

        private void DZFilter(object sender, FilterEventArgs e)
        {
            DomZdravlja dz = e.Item as DomZdravlja;
            if (dz != null)
            {
                e.Accepted = dz.Aktivan == true && dz.Adresa.Grad.Equals(cbGrad.SelectedItem)
                    && (dz.Naziv.Contains(TbPretraga.Text) || dz.Adresa.ToString().Contains(TbPretraga.Text));
            }
        }

        private void LekariFilter(object sender, FilterEventArgs e)
        {
            Lekar lekar = e.Item as Lekar;
            DomZdravlja dz = dgDZ.SelectedItem as DomZdravlja;
            if (lekar != null && dz != null)
            {
                e.Accepted = lekar.Korisnik.Aktivan == true && lekar.DomZdravlja.Naziv.Equals(dz.Naziv);
            }
            else if (lekar != null)
            {
                e.Accepted = lekar.DomZdravlja == null;
            }
        }

        private void cbGrad_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dgDZ.SelectedItem = null;
            cvs.View.Refresh();
            cvs1.View.Refresh();
        }

        private void dgDZ_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cvs1.View.Refresh();
        }
        

        private void BtnReg_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            RegistracijaWindow window = new RegistracijaWindow();
            window.Show();
        }

        private void BtnPrikazTermina_Click(object sender, RoutedEventArgs e)
        {
            if (dgLekari.SelectedItem != null)
            {
                Lekar lekar = dgLekari.SelectedItem as Lekar;
                TerminiWindow window = new TerminiWindow(lekar, pacijent);
                window.Show();
            }
            /*else
            {
                MessageBox.Show("Odaberite lekara.","!!!");
            }*/
        }

        private void BtnZK_Click(object sender, RoutedEventArgs e)
        {
            ZKWindow window = new ZKWindow(this.pacijent);
            window.Show();
        }

        private void BtnOdjava_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            HomeWindow window = new HomeWindow();
            window.Show();
        }

        private void dgDZ_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("ID"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void MIPodaci_Click(object sender, RoutedEventArgs e)
        {
            RegistracijaWindow window = new RegistracijaWindow(this.pacijent.Korisnik);
            window.Show();
        }

        private void TbPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            cvs.View.Refresh();
        }
    }
}
