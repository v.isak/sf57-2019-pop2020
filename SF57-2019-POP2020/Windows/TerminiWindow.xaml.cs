﻿using SF57_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for TerminiWindow.xaml
    /// </summary>
    public partial class TerminiWindow : Window
    {
        CollectionViewSource cvs = new CollectionViewSource();
        Lekar lekar;
        Pacijent pacijent;

        public TerminiWindow(Lekar lekar = null, Pacijent pacijent = null)
        {
            this.lekar = lekar;
            this.pacijent = pacijent;
            InitializeComponent();

            if (lekar != null && pacijent != null)
            {
                Title = "Lekar: " + lekar.Korisnik.KorisnickoIme;
                cvs.Source = Util.Instance.listaTermina;
                cvs.Filter += new FilterEventHandler(TerminiFilter);
                dgTermini.ItemsSource = cvs.View;
                dgTermini.AutoGenerateColumns = false;
                dgTermini.SelectedItem = null;
                dgTermini.IsReadOnly = true;
            }
        }

        private void TerminiFilter(object sender, FilterEventArgs e)
        {
            Termin termin = e.Item as Termin;
            if (termin != null)
            {
                if(DateTime.Compare(termin.Datum, DateTime.Now) < 0) // datum je prosao pa se pregled zavrsio ili se uopste nije desio
                {
                    termin.StatusTermina = EStatusTermina.Zavrsen;
                } 
                e.Accepted = termin.Lekar.Equals(this.lekar);
            }
        }

        private void btnZakaziTermin_Click(object sender, RoutedEventArgs e)
        {
            Termin termin = dgTermini.SelectedItem as Termin;
            if (termin != null)
            {
                if (termin.StatusTermina == EStatusTermina.Slobodan)
                {
                    MessageBoxResult odgovor = MessageBox.Show("Da li ste sigurni da zelite da zakazete ovaj termin?", "Potvrda", MessageBoxButton.YesNo);
                    if (odgovor == MessageBoxResult.Yes)
                    {
                        termin.StatusTermina = EStatusTermina.Zakazan;
                        termin.Pacijent = this.pacijent;
                        this.pacijent.ListaTermina.Add(termin);
                        cvs.View.Refresh();
                        MessageBox.Show("Uspesno ste zakazali termin!");

                    }
                }

                else
                {
                    MessageBox.Show("Nije moguce zakazati ovaj termin.");
                }
            }
            else
            {
                MessageBox.Show("Odaberite termin.");
            }
        }
    }
}
