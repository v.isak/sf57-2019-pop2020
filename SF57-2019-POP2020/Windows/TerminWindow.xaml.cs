﻿using SF57_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for TerminWindow.xaml
    /// </summary>
    public partial class TerminWindow : Window
    {
        Termin termin;
        public TerminWindow(Termin termin = null)
        {
            this.termin = termin;
            InitializeComponent();

            CbLekar.ItemsSource = Util.Instance.Lekari;
            if (termin != null)
            {
                TbID.Text = termin.ID;
                CbLekar.SelectedItem = termin.Lekar;
                DpDatum.SelectedDate = termin.Datum.Date;
                TbVreme.Text = termin.Datum.ToString("HH:mm:ss");
            }
        }

        private void BtnPotvrda_Click(object sender, RoutedEventArgs e)
        {
            if (!greska())
            {
                DateTime datum = (DateTime)DpDatum.SelectedDate;
                TimeSpan vreme = TimeSpan.Parse(TbVreme.Text);
                datum = datum.Add(vreme);

                if (this.termin != null)
                {
                    if (Util.Instance.listaTermina.ToList().Find(k => k.Datum.ToString("HH:mm:ss").Equals(datum.ToString("HH:mm:ss")) 
                    && k.Lekar == this.termin.Lekar && k != this.termin) != null)
                    {
                        MessageBox.Show("Ovaj datum je vec zakazan za lekara.", "Greska");
                    }
                    else
                    {
                        this.termin.ID = TbID.Text;
                        this.termin.Lekar = CbLekar.SelectedItem as Lekar;
                        this.termin.Datum = datum;
                        this.Close();
                    }
                }

                else
                {
                    if (Util.Instance.listaTermina.ToList().Find(k => k.Datum.ToString("HH:mm:ss").Equals(datum.ToString("HH:mm:ss")) && k.Lekar == this.termin.Lekar) != null)
                    {
                        MessageBox.Show("Ovaj datum je vec zakazan za lekara.", "Greska");
                    }
                    else
                    {
                        Termin termin = new Termin
                        {
                            ID = TbID.Text,
                            Lekar = CbLekar.SelectedItem as Lekar,
                            Datum = datum,
                            StatusTermina = EStatusTermina.Slobodan,
                            Pacijent = null,
                            Aktivan = true
                        };
                        Util.Instance.listaTermina.Add(termin);
                        this.Close();
                    }
                }
            }
        }

        private bool greska()
        {
            DateTime sada = DateTime.Today;
            DateTime selektovani = new DateTime();
            if (DpDatum.SelectedDate != null)
            {
                selektovani = (DateTime)DpDatum.SelectedDate;
            }

            bool greska = false;
            string msg = "Popravite sledece:";

            if (string.IsNullOrWhiteSpace(TbID.Text))
            {
                greska = true;
                msg += "\nUnesite ID.";
            }
            if (CbLekar.SelectedItem == null)
            {
                greska = true;
                msg += "\nUnesite lekara.";
            }
            if (DpDatum.SelectedDate != null)
            {
                if (DateTime.Compare(sada, selektovani) == 0)
                {
                    if (TimeSpan.TryParse(TbVreme.Text, out TimeSpan vreme) == true)
                    {
                        /*Console.WriteLine(sada.Add(vreme));
                        Console.WriteLine(DateTime.Now);*/
                        if (DateTime.Compare(DateTime.Now, sada.Add(vreme)) == 0 || DateTime.Compare(DateTime.Now, sada.Add(vreme)) > 0)
                        {
                            greska = true;
                            msg += "\nUnesite validno vreme.";
                        }
                    }
                }
                else if (DateTime.Compare(sada, selektovani) > 0)
                {
                    greska = true;
                    msg += "\nSelektujte validan datum.";
                }
            }
            else
            {
                greska = true;
                msg += "\nSelektujte datum.";
            }
            if (string.IsNullOrWhiteSpace(TbVreme.Text) == true)
            {
                greska = true;
                msg += "\nUnesite vreme.";
            }
            else
            {
                if (TimeSpan.TryParse(TbVreme.Text, out TimeSpan vreme) != true)
                {
                    greska = true;
                    msg += "\nUnesite korektan format vremena.";
                }
            }

            if (greska == true)
            {
                MessageBox.Show(msg, "Greska");
            }
            return greska;
        }
    }
}
