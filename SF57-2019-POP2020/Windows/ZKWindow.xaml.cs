﻿using SF57_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for ZKWindow.xaml
    /// </summary>
    public partial class ZKWindow : Window
    {
        CollectionViewSource cvs = new CollectionViewSource();
        Pacijent pacijent;
        Lekar lekar;
        public ZKWindow(Pacijent pacijent, Lekar lekar = null)
        {
            this.pacijent = pacijent;
            this.lekar = lekar;
            InitializeComponent();

            Title = pacijent.Korisnik.KorisnickoIme;
            lblJMBG.Content += pacijent.Korisnik.JMBG;

            cvs.Source = pacijent.ZdravstveniKarton;
            cvs.Filter += new FilterEventHandler(PretragaFilter);
            dgTerapije.ItemsSource = cvs.View;
            dgTerapije.AutoGenerateColumns = false;
            dgTerapije.SelectedItem = null;
            dgTerapije.IsReadOnly = true;
            
            if (lekar != null)
            {
                BtnDodajTerapiju.Visibility = Visibility.Visible;
            }

        }

        private void dgTerapije_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Terapija terapija = dgTerapije.SelectedItem as Terapija;
            TerapijaWindow window = new TerapijaWindow(terapija);
            window.Show();
        }

        private void BtnDodajTerapiju_Click(object sender, RoutedEventArgs e)
        {
            TerapijaWindow window = new TerapijaWindow(null, this.pacijent, this.lekar);
            window.Show();
        }

        private void PretragaFilter(object sender, FilterEventArgs e)
        {
            Terapija terapija = e.Item as Terapija;
            e.Accepted = terapija.Aktivan == true && (terapija.Opis.Contains(TbPretraga.Text) || terapija.Lekar.Korisnik.KorisnickoIme.Contains(TbPretraga.Text));
        }
        private void TbPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            cvs.View.Refresh();
        }
    }
}
