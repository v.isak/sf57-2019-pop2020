﻿using SF57_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for DZWindow.xaml
    /// </summary>
    public partial class DZWindow : Window
    {
        DomZdravlja DZ;
        public DZWindow(DomZdravlja DZ = null)
        {
            this.DZ = DZ;
            this.DataContext = DZ;
            InitializeComponent();

            if (this.DZ != null)
            {
                BtnPotvrda.Visibility = Visibility.Hidden;
                TbAdresa.Text = DZ.Adresa.ToString();
            }
        }

        private void btnAdresa_Click(object sender, RoutedEventArgs e)
        {
            AdresaWindow window;
            if (this.DZ != null)
            {
                window = new AdresaWindow(null, DZ.Adresa, this);
            }
            else
            {
                window = new AdresaWindow(null, null, this);
            }
            window.Show();
        }

        private void BtnPotvrda_Click(object sender, RoutedEventArgs e)
        {
            if (!greska())
            {
                if (this.DZ != null)
                {
                    this.DZ.ID = TbID.Text;
                    this.DZ.Naziv = TbNaziv.Text;
                }
                else
                {
                    string[] ad = TbAdresa.Text.Split(',');
                    Adresa adresa = new Adresa
                    {
                        Drzava = ad[0],
                        Grad = ad[1],
                        Ulica = ad[2],
                        Broj = ad[3]
                    };
                    DomZdravlja dz = new DomZdravlja
                    {
                        ID = TbID.Text,
                        Naziv = TbNaziv.Text,
                        Adresa = adresa,
                        Aktivan = true
                    };
                    Util.Instance.listaDomovaZdravlja.Add(dz);
                    Util.Instance.listaAdresa.Add(adresa);
                    Util.Instance.listaGradova.Add(adresa.Grad);
                }

                this.Close();
            }
        }

        private bool greska()
        {
            string msg = "Popravite sledece:";
            bool greska = false;

            if (string.IsNullOrWhiteSpace(TbID.Text))
            {
                greska = true;
                msg += "\nUnesite ID.";
            }
            if (Util.Instance.listaDomovaZdravlja.ToList().Find(k => k.ID.Equals(TbID.Text)) != null)
            {
                greska = true;
                msg += "\nOvaj ID vec postoji.";
            }
            if (string.IsNullOrWhiteSpace(TbNaziv.Text))
            {
                greska = true;
                msg += "\nUnesite naziv.";
            }
            if (string.IsNullOrWhiteSpace(TbAdresa.Text))
            {
                greska = true;
                msg += "\nUnesite adresu.";
            }

            if (greska == true)
            {
                MessageBox.Show(msg, "Greska");
            }

            return greska;
        }
    }
}
