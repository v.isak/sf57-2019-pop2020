﻿using SF57_2019_POP2020.Models;
using SF57_2019_POP2020.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for TerapijaWindow.xaml
    /// </summary>
    public partial class TerapijaWindow : Window
    {
        Terapija terapija;
        Pacijent pacijent;
        Lekar lekar;
        Administrator admin;
        public TerapijaWindow(Terapija terapija = null, Pacijent pacijent = null, Lekar lekar = null, Administrator admin = null)
        {
            this.terapija = terapija;
            this.pacijent = pacijent;
            this.lekar = lekar;
            this.admin = admin;
            InitializeComponent();

            if (terapija != null)
            { // readonly pregled terapije
                tbTerapija.Text = terapija.Opis;

                if (admin != null)
                {
                    tbTerapija.IsReadOnly = false;
                    BtnPotvrda.Visibility = Visibility.Visible;
                }
            }
            else
            { // dodavanje
                if (lekar != null || admin != null)
                {
                    tbTerapija.IsReadOnly = false;
                    BtnPotvrda.Visibility = Visibility.Visible;

                    if (admin != null)
                    {
                        CbPacijenti.Visibility = Visibility.Visible;

                        List<Pacijent> pacijenti = Util.Instance.Pacijenti.ToList().FindAll(k => k.Korisnik.Aktivan == true);
                        if (pacijenti.Count() != 0) 
                        {
                            CbPacijenti.ItemsSource = pacijenti;
                            CbPacijenti.SelectedIndex = 0;
                        }
                    }
                }
            }
        }

        private void BtnPotvrda_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbTerapija.Text))
            {
                MessageBox.Show("Unesite opis terapije!", "Greska");
            }
            else
            {
                if (terapija == null)
                {
                    Terapija terapija = new Terapija
                    {
                        ID = (double.Parse(Util.Instance.ID) + 1).ToString(),
                        Lekar = this.lekar,
                        Opis = tbTerapija.Text,
                        Aktivan = true
                    };

                    if (admin == null)
                    {
                        this.pacijent.ZdravstveniKarton.Add(terapija);
                    }
                    else
                    {
                        Pacijent pacijent = CbPacijenti.SelectedItem as Pacijent;
                        if (pacijent != null)
                        {
                            pacijent.ZdravstveniKarton.Add(terapija);
                            Util.Instance.listaTerapija.Add(terapija);
                        }
                        else
                        {
                            MessageBox.Show("Ne postoje pacijenti za dodavanje terapije.", "Greska");
                            this.Close();
                        }
                    }                
                }
                else
                {
                    terapija.Opis = tbTerapija.Text;
                }
                this.Close();
            }
        }
    }
}
