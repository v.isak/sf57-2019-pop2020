﻿using SF57_2019_POP2020.Models;
using SF57_2019_POP2020.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        Administrator admin;
        CollectionViewSource cvs = new CollectionViewSource();
        public AdminWindow(Administrator admin)
        {
            this.admin = admin;
            InitializeComponent();
            CbZaIzmenu.ItemsSource = new ObservableCollection<string> { "Korisnici", "Domovi zdravlja", "Termini", "Terapije" };
            Title += " " + admin.Korisnik.KorisnickoIme;
            cvs.Filter += new FilterEventHandler(AktivniFilter);
            cvs.Filter += new FilterEventHandler(PretragaFilter);
            DgSelektovani.AutoGenerateColumns = true;
            DgSelektovani.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void AktivniFilter(object sender, FilterEventArgs e)
        {
            if (CbZaIzmenu.SelectedItem.ToString() == "Korisnici" && e.Item is Korisnik)
            {
                Korisnik korisnik = e.Item as Korisnik;
                e.Accepted = korisnik.Aktivan == true;
            }
            else if (CbZaIzmenu.SelectedItem.ToString() == "Domovi zdravlja" && e.Item is DomZdravlja)
            {
                DomZdravlja dz = e.Item as DomZdravlja;
                e.Accepted = dz.Aktivan == true;
            }
            else if (CbZaIzmenu.SelectedItem.ToString() == "Termini" && e.Item is Termin)
            {
                Termin termin = e.Item as Termin;
                e.Accepted = termin.Aktivan == true;
            }
            else if (CbZaIzmenu.SelectedItem.ToString() == "Terapije" && e.Item is Terapija)
            {
                Terapija terapija = e.Item as Terapija;
                e.Accepted = terapija.Aktivan == true;
            }
        }

        private void MIPodaci_Click(object sender, RoutedEventArgs e)
        {
            RegistracijaWindow window = new RegistracijaWindow(this.admin.Korisnik);
            window.Show();
        }

        private void BtnOdjava_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            HomeWindow window = new HomeWindow();
            window.Show();
        }

        private void BtnZaIzmenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updateView();
        }

        private void updateView()
        {
            if (CbZaIzmenu.SelectedItem.ToString() == "Korisnici")
            {
                cvs.Source = admin.ListaKorisnika;
            }
            else if (CbZaIzmenu.SelectedItem.ToString() == "Domovi zdravlja")
            {
                cvs.Source = admin.ListaDZ;
            }
            else if (CbZaIzmenu.SelectedItem.ToString() == "Termini")
            {
                cvs.Source = admin.ListaTermina;
            }
            else if (CbZaIzmenu.SelectedItem.ToString() == "Terapije")
            {
                cvs.Source = admin.ListaTerapija;
            }
            DgSelektovani.ItemsSource = cvs.View;
            DgSelektovani.SelectedItem = null;
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (CbZaIzmenu.SelectedItem == null) { }
            else if (CbZaIzmenu.SelectedItem.ToString() == "Korisnici")
            {
                if (DgSelektovani.SelectedItem != null)
                {
                    MessageBoxResult odgovor = MessageBox.Show("Da li ste sigurni da zelite da obrisete ovog korisnika?", "Potvrda", MessageBoxButton.YesNo);
                    if (MessageBoxResult.Yes == odgovor)
                    {
                        Korisnik korisnik = DgSelektovani.SelectedItem as Korisnik;
                        korisnik.Aktivan = false;
                        cvs.View.Refresh();

                        // ODJAVA, ako je obrisan admin ulogovan admin
                        if (korisnik == this.admin.Korisnik)
                        {
                            this.Close();
                            HomeWindow window = new HomeWindow();
                            window.Show();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Selektujte korisnika za brisanje.", "Greska");
                }
            }
            else if (CbZaIzmenu.SelectedItem.ToString() == "Domovi zdravlja")
            {
                if (DgSelektovani.SelectedItem != null)
                {
                    MessageBoxResult odgovor = MessageBox.Show("Da li ste sigurni da zelite da obrisete ovaj dom zdravlja?", "Potvrda", MessageBoxButton.YesNo);
                    if (MessageBoxResult.Yes == odgovor)
                    {
                        DomZdravlja dz = DgSelektovani.SelectedItem as DomZdravlja;
                        dz.Aktivan = false;
                        cvs.View.Refresh();
                    }
                }
                else
                {
                    MessageBox.Show("Selektujte dom zdravlja za brisanje.", "Greska");
                }
            }
            else if (CbZaIzmenu.SelectedItem.ToString() == "Termini")
            {
                if (DgSelektovani.SelectedItem != null)
                {
                    MessageBoxResult odgovor = MessageBox.Show("Da li ste sigurni da zelite da obrisete ovaj termin?", "Potvrda", MessageBoxButton.YesNo);
                    if (MessageBoxResult.Yes == odgovor)
                    {
                        Termin termin = DgSelektovani.SelectedItem as Termin;
                        termin.Aktivan = false;
                        cvs.View.Refresh();
                    }
                }
                else
                {
                    MessageBox.Show("Selektujte termin za brisanje.", "Greska");
                }
            }
            else if (CbZaIzmenu.SelectedItem.ToString() == "Terapije")
            {
                if (DgSelektovani.SelectedItem != null)
                {
                    MessageBoxResult odgovor = MessageBox.Show("Da li ste sigurni da zelite da obrisete ovu terapiju?", "Potvrda", MessageBoxButton.YesNo);
                    if (MessageBoxResult.Yes == odgovor)
                    {
                        Terapija terapija = DgSelektovani.SelectedItem as Terapija;
                        terapija.Aktivan = false;
                        cvs.View.Refresh();
                    }
                }
                else
                {
                    MessageBox.Show("Selektujte terapiju za brisanje.", "Greska");
                }
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (CbZaIzmenu.SelectedItem != null) {
                if (CbZaIzmenu.SelectedItem.ToString() == "Korisnici")
                {
                    RegistracijaWindow window = new RegistracijaWindow(null,this.admin);
                    window.ShowDialog();

                }
                else if (CbZaIzmenu.SelectedItem.ToString() == "Domovi zdravlja")
                {
                    DZWindow window = new DZWindow();
                    window.ShowDialog();
                }
                else if (CbZaIzmenu.SelectedItem.ToString() == "Termini")
                {
                    TerminWindow window = new TerminWindow();
                    window.ShowDialog();
                }
                else if (CbZaIzmenu.SelectedItem.ToString() == "Terapije")
                {
                    TerapijaWindow window = new TerapijaWindow(null, null, null, this.admin);
                    window.ShowDialog();
                }
                cvs.View.Refresh();
            }
        }

        private void DgSelektovani_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (CbZaIzmenu.SelectedItem != null)
            {
                if (CbZaIzmenu.SelectedItem.ToString() == "Korisnici")
                {
                    if (DgSelektovani.SelectedItem != null)
                    {
                        RegistracijaWindow window = new RegistracijaWindow(DgSelektovani.SelectedItem as Korisnik, this.admin);
                        window.ShowDialog();
                    }
                }
                else if (CbZaIzmenu.SelectedItem.ToString() == "Domovi zdravlja")
                {
                    DZWindow window = new DZWindow(DgSelektovani.SelectedItem as DomZdravlja);
                    window.ShowDialog();
                }
                else if (CbZaIzmenu.SelectedItem.ToString() == "Termini")
                {
                    TerminWindow window = new TerminWindow(DgSelektovani.SelectedItem as Termin);
                    window.ShowDialog();
                }
                else if (CbZaIzmenu.SelectedItem.ToString() == "Terapije")
                {
                    if (DgSelektovani.SelectedItem != null)
                    {
                        Terapija terapija = DgSelektovani.SelectedItem as Terapija;
                        TerapijaWindow window = new TerapijaWindow(terapija, null, null, this.admin);
                        window.ShowDialog();
                    }
                }
                cvs.View.Refresh();
            }
        }

        private void PretragaFilter(object sender, FilterEventArgs e)
        {
            if (TbPretraga.Text != "")
            {
                if (CbZaIzmenu.SelectedItem.ToString() == "Korisnici" && e.Item is Korisnik)
                {
                    Korisnik korisnik = e.Item as Korisnik;
                    e.Accepted = korisnik.Aktivan == true && (korisnik.Ime.Contains(TbPretraga.Text) || korisnik.Prezime.Contains(TbPretraga.Text) || korisnik.Email.Contains(TbPretraga.Text)
                        || korisnik.Adresa.ToString().Contains(TbPretraga.Text) || korisnik.TipKorisnika.ToString().ToLower().Contains(TbPretraga.Text.ToLower()));
                        
                }
                else if (CbZaIzmenu.SelectedItem.ToString() == "Domovi zdravlja" && e.Item is DomZdravlja)
                {
                    DomZdravlja dz = e.Item as DomZdravlja;
                    e.Accepted = dz.Aktivan == true && (dz.Naziv.Contains(TbPretraga.Text) || dz.Adresa.ToString().Contains(TbPretraga.Text));
                }
                /*else if (CbZaIzmenu.SelectedItem.ToString() == "Termini" && e.Item is Termin)
                {
                    Termin termin = e.Item as Termin;
                    e.Accepted = termin.Aktivan == true;
                }*/
                else if (CbZaIzmenu.SelectedItem.ToString() == "Terapije" && e.Item is Terapija)
                {
                    Terapija terapija = e.Item as Terapija;
                    e.Accepted = terapija.Aktivan == true && (terapija.Opis.Contains(TbPretraga.Text) || terapija.Lekar.Korisnik.KorisnickoIme.Contains(TbPretraga.Text));
                }
            }
        }

        private void TbPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            cvs.View.Refresh();
        }
    }
}
