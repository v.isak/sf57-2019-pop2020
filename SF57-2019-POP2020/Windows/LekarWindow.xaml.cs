﻿using SF57_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for LekarWindow.xaml
    /// </summary>
    public partial class LekarWindow : Window
    {
        readonly Korisnik korisnik;
        readonly CollectionViewSource cvs = new CollectionViewSource();
        readonly CollectionViewSource cvs1 = new CollectionViewSource();
        public LekarWindow(Korisnik korisnik)
        {
            
            this.korisnik = korisnik;
            InitializeComponent();
            this.Title += korisnik.KorisnickoIme;

            cbGrad.ItemsSource = Util.Instance.listaGradova;

            cvs.Source = Util.Instance.listaDomovaZdravlja;
            cvs.Filter += new FilterEventHandler(DzFilter);
            dgDZ.ItemsSource = cvs.View;
            dgDZ.AutoGenerateColumns = true;
            dgDZ.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            cvs1.Source = Util.Instance.listaTermina;
            cvs1.Filter += new FilterEventHandler(TerminiFilter);
            dgTermini.ItemsSource = cvs1.View;
            dgTermini.AutoGenerateColumns = true;
            dgTermini.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            if (korisnik.TipKorisnika == ETipKorisnika.LEKAR)
            {
                   
                // window lekar
            }
        }

        private void DzFilter(object sender, FilterEventArgs e)
        {
            DomZdravlja dz = e.Item as DomZdravlja;
            if (dz != null)
            {
                e.Accepted = dz.Aktivan == true && dz.Adresa.Grad.Equals(cbGrad.SelectedItem);
            }
        }

        private void TerminiFilter(object sender, FilterEventArgs e)
        {
            Termin termin = e.Item as Termin;
            DomZdravlja dz = dgDZ.SelectedItem as DomZdravlja;
            if (termin != null && dz != null && dpDatum.SelectedDate != null)
            {
                e.Accepted = termin.Lekar.DomZdravlja.Naziv.Equals(dz.Naziv) && termin.Datum.Date == dpDatum.SelectedDate && termin.Aktivan == true;
            }
            else if (termin != null && dz != null){
                e.Accepted = termin.Lekar.DomZdravlja.Naziv.Equals(dz.Naziv) && termin.Aktivan == true;
            }
            else if (termin != null)
            {
                e.Accepted = termin.Lekar.DomZdravlja == null && termin.Aktivan == true;
            }
        }

        private void BtnOdjava_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            HomeWindow window = new HomeWindow();
            window.Show();
        }

        private void MIPodaci_Click(object sender, RoutedEventArgs e)
        {
            RegistracijaWindow window = new RegistracijaWindow(this.korisnik);
            window.Show();
        }

        private void dgDZ_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cvs1.View.Refresh();
            BtnObrisiTermin.Visibility = Visibility.Hidden;
        }

        private void cbGrad_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dgDZ.SelectedItem = null;
            cvs.View.Refresh();
            cvs1.View.Refresh();
        }

        private void dgDZ_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("ID"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void dgTermini_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("ID"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void dgTermini_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Termin termin = dgTermini.SelectedItem as Termin;
            if (termin != null)
            {
                if (termin.StatusTermina == EStatusTermina.Zakazan && termin.Pacijent != null)
                {
                    Lekar lekar = Util.Instance.Lekari.ToList().Find(k => k.Korisnik.KorisnickoIme.Equals(this.korisnik.KorisnickoIme));
                    ZKWindow zk = new ZKWindow(termin.Pacijent, lekar);
                    zk.Show();
                }
                else
                {
                    MessageBox.Show("Ne postoji zdravstveni karton!", "Greska");
                }
            }
        }

        private void dpDatum_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            cvs1.View.Refresh();
        }



        private void BtnObrisiTermin_Click(object sender, RoutedEventArgs e)
        {
            if (dgTermini.SelectedItem != null)
            {
                MessageBoxResult odgovor = MessageBox.Show("Da li ste sigurni da zelite da obrisete ovaj termin?", "Potvrda", MessageBoxButton.YesNo);
                if (MessageBoxResult.Yes == odgovor)
                {
                    Termin termin = dgTermini.SelectedItem as Termin;
                    termin.Aktivan = false;
                    BtnObrisiTermin.Visibility = Visibility.Hidden;
                    cvs1.View.Refresh();
                }
            }
            else
            {
                MessageBox.Show("Selektujte termin za brisanje.", "Greska");
            }
        }

        private void dgTermini_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgTermini.SelectedItem != null)
            {
                Termin termin = dgTermini.SelectedItem as Termin;
                if (termin.StatusTermina == EStatusTermina.Slobodan)
                {
                    BtnObrisiTermin.Visibility = Visibility.Visible;
                }
            }
        }
    }
}
