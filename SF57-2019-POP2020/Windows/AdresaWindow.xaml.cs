﻿using SF57_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF57_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AdresaWindow.xaml
    /// </summary>
    public partial class AdresaWindow : Window
    {
        Adresa adresa;
        RegistracijaWindow window;
        DZWindow dzWindow;
        public AdresaWindow(RegistracijaWindow window = null,Adresa adresa = null, DZWindow dzWindow = null)
        {
            this.adresa = adresa;
            this.DataContext = adresa;
            this.dzWindow = dzWindow;
            this.window = window;
            InitializeComponent();
            if (adresa != null)
            {
                txtDrzava.Text = adresa.Drzava;
                txtGrad.Text = adresa.Grad;
                txtUlica.Text = adresa.Ulica;
                txtBroj.Text = adresa.Broj;
            }
        }

        private void btnPotvrda_Click(object sender, RoutedEventArgs e)
        {
            if (!greska())
            {
                if (this.adresa != null)
                {
                    this.adresa.Drzava = txtDrzava.Text;
                    this.adresa.Grad = txtGrad.Text;
                    this.adresa.Ulica = txtUlica.Text;
                    this.adresa.Broj = txtBroj.Text;
                }
                string adresa = txtDrzava.Text + "," + txtGrad.Text + "," + txtUlica.Text + "," + txtBroj.Text;
                if (window != null)
                {
                    window.txtAdresa.Text = adresa;
                }
                else if (dzWindow != null)
                {
                    dzWindow.TbAdresa.Text = adresa;
                }
                this.Close();
            }
        }

        private bool greska()
        {
            bool validanBroj = Double.TryParse(txtBroj.Text, out double result);
            string msg = "Popravite sledece:";
            bool greska = false;
            if (txtDrzava.Text.Contains(",") || txtGrad.Text.Contains(",") || txtUlica.Text.Contains(",") || txtBroj.Text.Contains(","))
            {
                greska = true;
                msg += "\nUklonite karakter ','";
            }
            if (txtDrzava.Text.Contains(";") || txtGrad.Text.Contains(";") || txtUlica.Text.Contains(";") || txtBroj.Text.Contains(";"))
            {
                greska = true;
                msg += "\nUklonite karakter ';'";
            }
            if (txtDrzava.Text.Trim(' ').Equals("") || txtGrad.Text.Trim(' ').Equals("") || txtUlica.Text.Trim(' ').Equals("") || txtBroj.Text.Trim(' ').Equals(""))
            {
                greska = true;
                msg += "\nUnesite validnu adresu.";
            }
            if (!validanBroj && txtBroj.Text.Length > 0)
            {
                greska = true;
                msg += "\nUnesite korektan broj.";
            }

            if (greska == true)
            {
                MessageBox.Show(msg, "Greska");
            }
            return greska;
        }
    }
}
